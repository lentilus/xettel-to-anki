from pydantic import BaseModel
from genanki import Model

class FlashCard(BaseModel):
    question: str
    source: str

class LatexModel(Model):
    def __init__(self, latex_pre = None, latex_post = None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        print(*args)
        print("init")
        self.latex_pre = latex_pre
        self.latex_post = latex_post

    def to_json(self, *args, **kwargs):
        json = super().to_json(*args, **kwargs)
        if self.latex_pre is not None:
            json['latexPre'] = self.latex_pre
        if self.latex_post is not None:
            json['latexPost'] = self.latex_post

        return json
