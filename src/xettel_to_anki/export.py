import genanki
import subprocess
import sys
import os
from typing import List

from xettel_to_anki.parser import parse_flashcards
from xettel_to_anki.models import LatexModel

def zettel_model(preamble: str, id: int = 2491559367)->LatexModel:
    pre = r"""
    \documentclass[12pt]{article}
    \usepackage[paperwidth=5in, paperheight=100in]{geometry}
    \pagestyle{empty}
    """ + preamble +r"""
    \begin{document}
    """

    post = r"\end{document}"

    model = LatexModel(
                pre,
                post,
                id,
                "latex_model",
                fields=[{"name": "Name"}, {"name": "Question"}, {"name": "Answer"}],
                templates=[{"name": "{{Name}}", "qfmt": "[latex]{{Question}}[/latex]", "afmt": "[latex]{{Answer}}[/latex]"}],
            )
    return model

def create_deck(preamble_file: str, tex_files: List[str],  id: int = 1391554143): 
    deck = genanki.Deck(id, "zettelkasten")

    with open(os.path.join(preamble_file), 'r') as f:
        preamble = f.read()

    model = zettel_model(preamble)

    for zettel in tex_files:
        if zettel == "":
            continue

        try:
            with open(os.path.join(zettel, "zettel.tex"), 'r') as f:
                source = f.read()
                print(f" searching in {zettel}")

            cards = parse_flashcards(source)
            card_counter=1
            for card in cards:
                print("adding flashcard...")
                question = card.question if card.question != "" else f"\\textbf{{ {os.path.basename(zettel).replace('_', ' ')} }}"
                answer = card.source
                note = genanki.Note(model=model, fields=[f"{os.path.basename(zettel)} - {card_counter}", question, answer])
                deck.add_note(note)
                card_counter+=1
        except:
            continue
    return deck

def main():
    apkg = sys.argv[1]
    preamble_file = sys.argv[2]
    tex_files=[]
    for arg in sys.argv[3:]:
        tex_files.append(arg)

    deck = create_deck(preamble_file, tex_files)
    genanki.Package(deck).write_to_file(apkg)

if __name__ == '__main__':
    main()
