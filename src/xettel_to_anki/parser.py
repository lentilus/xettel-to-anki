from typing import List
from pylatexenc.latexwalker import LatexWalker, LatexEnvironmentNode, LatexNode
from xettel_to_anki.models import FlashCard
import sys
import json

def recurse_env(nodes: List[LatexNode], name: str)-> List[LatexEnvironmentNode]:
    found= []
    for node in nodes:
        if isinstance(node, LatexEnvironmentNode):
            if node.environmentname == name:
                found.append(node)
                continue

        # not very beautiful, but gets the job done
        try:
            found += recurse_env(node.nodelist, name)
        except:
            pass
    return found

def parse_flashcards(src: str, env: str = "flashcard", question_env: str = "question")-> List[FlashCard]:
    nodes, _, _ = LatexWalker(src).get_latex_nodes()
    filtered = recurse_env(nodes, env)
    flashcards: List[FlashCard] = []

    for card in filtered:
        cstart = card.pos
        cstop = card.len + cstart
        card_code = src[cstart:cstop]

        question = recurse_env(card.nodelist, question_env)
        if len(question) == 0:
            question_code=""
        else:
            qstart = question[0].pos
            qstop =  question[0].len + qstart
            question_code = src[qstart:qstop]

        flashcard = FlashCard(question=question_code, source=card_code)
        flashcards.append(flashcard)
    return flashcards

def main():
    source = sys.argv[1]
    cards = parse_flashcards(source)
    card_json = []
    for card in cards:
        card_json.append(card.model_dump(mode='json'))

    json.dump(card_json, sys.stdout)

if __name__ == '__main__':
    main()

